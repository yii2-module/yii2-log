/**
 * Database schema structure required by Yii2Module\Yii2Log\Yii2LogModule
 * 
 * @author Anastaszor
 * @license MIT
 */

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log`
(
	`id` BIGINT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The id of the log event',
	`log_time` DOUBLE NOT NULL COMMENT 'The timestamp when this event was registered',
	`level` VARCHAR(128) NOT NULL DEFAULT 'UNKNOWN' COLLATE ascii_bin COMMENT 'The severity of this event',
	`category` VARCHAR(128) NOT NULL DEFAULT 'UNKNOWN' COLLATE ascii_bin COMMENT 'The category of the event',
	`prefix` VARCHAR(255) NOT NULL DEFAULT 'UNKNOWN' COLLATE ascii_bin COMMENT 'The subcategory of the event',
	`message` MEDIUMBLOB NOT NULL COMMENT 'The full details of the event',
	INDEX `idx_log_time` (`log_time`)
)
ENGINE=MyISAM DEFAULT CHARSET=ascii;
