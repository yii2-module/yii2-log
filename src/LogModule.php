<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-log library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Log;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2Log\Models\Log;

/**
 * LogModule class file.
 * 
 * This module is made to offer an api over the logging table.
 * 
 * @author Anastaszor
 */
class LogModule extends BootstrappedModule
{
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'file-text';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('LogModule.Module', 'Log');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'log' => new Bundle(BaseYii::t('LogModule.Module', 'Log'), [
				'log' => (new Record(Log::class, 'log', BaseYii::t('LogModule.Module', 'Log')))->enableFullAccess(),
			]),
		];
	}
	
}
