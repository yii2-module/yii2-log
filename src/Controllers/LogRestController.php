<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-log library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Log\Controllers;

use yii\rest\ActiveController;
use Yii2Module\Yii2Log\Models\Log;

/**
 * LogController class file.
 * 
 * This class represents the 
 * 
 * @author Anastaszor
 */
class LogRestController extends ActiveController
{
	
	public $modelClass = Log::class;
	
}
