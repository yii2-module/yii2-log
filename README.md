# yii2-module/yii2-log

A module that manages database-based log records for centralisation purposes.

![coverage](https://gitlab.com/yii2-module/yii2-log/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-log/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-log ^8`


## Basic Usage

This module needs the following components to be set at the configuration level:

- 'db_log' should be a `\yii\db\Connection`

If you already have a database connection, you may use the following trick :

`'db_log' => function() { return \Yii::$app->get('db'); },`

where 'db' is the id of your database connection.

This module needs the following parameters to be set at the configuration level:

- none

Then the module should be configured as follows :

```php
$config = [
	...
	'modules' => [
		...
		'logger' => [ // cant use the 'log' component, already taken
			'class' => 'Yii2Module\Yii2Log\LogModule',
		],
		...
	],
	...
];
```


## License

MIT (See [license file](LICENSE)).
